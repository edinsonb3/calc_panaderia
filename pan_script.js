var elementoBase = 0;
var harina = 0;
var agua = 0;
var sal =0;
var levadura =0;
var grasa =0;
var masaTotal =0;
var masa =0;
var numSel =0;
var printLevadura;

alert("Bienvenido a la calculadora panadera");
alert("Empecemos seleccionando desde cual elemento queremos calcular los ingredientes: 1=MASA  o  2=HARINA");

while (numSel!=1 || numSel!=2){
    numSel = prompt('Ingrese el numero "1" para MASA o ingrese el numero "2" para calcular en base a la HARINA');
}


if (numSel == 1){
    elementoBase = "Masa";
    alert("Usted selecciono la " + elementoBase);
    while (masa==0){
            masa = prompt("Ingrese la cantidad de Masa que desea trabajar en gramos");
    }
    harina = masa/100*60;
    agua = harina/100*60;
    sal = harina/100*1;
    levadura = Math.round(harina/100*3);
    grasa = harina/100*7;
    masaTotal = harina+agua+sal+levadura+grasa;
    }

if (numSel == 2){
    elementoBase = "Harina";
    alert("Usted selecciono la " + elementoBase);
    while (harina==0){
        harina = prompt("Ingrese la cantidad de Harina que desea trabajar en gramos");
    } 
    agua = harina/100*60;
    sal = harina/100*1;
    levadura = Math.round(harina/100*3);
    grasa = harina/100*7;
    masaTotal = harina*1+agua+sal+levadura+grasa; 
    }

function printMasa(){
    if (elementoBase=="Masa"){
        document.write(`<br><br>Cantidad de masa que usted ingreso: <b>${Math.round(masa)} gramos.</b>`);
    }
    document.write(`<br><br>Cantidad de<b> HARINA</b> que debe agregar: <b>${Math.round(harina)} gramos.</b>`);
    document.write(`<br><br>Cantidad de<b> AGUA</b> que debe agregar: <b>${Math.round(agua)} gramos.</b>`);
    document.write(`<br><br>Cantidad de<b> SAL</b> que debe agregar: <b>${Math.round(sal)} gramos.</b>`);
    document.write(`<br><br>Cantidad de<b> LEVADURA</b> que debe agregar: <b>${levadura} gramos.</b>`);
    document.write(`<br><br>Cantidad de<b> GRASA</b> que debe agregar:   <b>  ${Math.round(grasa)} gramos.</b>`);
    document.write(`<br><br>Peso final de la mezcla segun la formula: <b>${Math.round(masaTotal)} gramos.</b>`);
}

